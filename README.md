# Index Updater

Подкладывает в целевую директорию на удаленной тачке индекс нужного поколения.<br> Поколение задается в переменной ```$INDEX_GENERATION```.
Возможные значения: ```1, 2```
<br>
Тригернуть ci/cd можно с помощью [trigger](https://gitlab.com/api/v4/projects/38914392/ref/main/trigger/pipeline?token=glptt-d75567ff0c2a052a7c0b7f0dd3156d099dc7fa98
) или запроса:
<br>
```bash
curl -X POST \
     --fail \
     -F token=glptt-d75567ff0c2a052a7c0b7f0dd3156d099dc7fa98 \
     -F ref=main \
     -F "variables[INDEX_GENERATION]=1" \
     https://gitlab.com/api/v4/projects/38914392/trigger/pipeline
```
